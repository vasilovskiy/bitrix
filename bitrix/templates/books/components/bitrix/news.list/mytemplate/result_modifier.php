<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die;
}

$dbResSect = CIBlockSection::GetList(
    Array("SORT" => "ASC"),
    Array("IBLOCK_ID" => $arParams['IBLOCK_ID'])
);

$dbResElem = CIBlockElement::GetList(
    Array("SORT" => "ASC"),
    Array("IBLOCK_ID" => $arParams['IBLOCK_ID'])
);

//Получаем разделы и собираем в массив
while ($sectRes = $dbResSect->GetNext()) {
    $arSections[] = $sectRes;
}

//Получаем элементы и собираем в массив
while ($elemRes = $dbResElem->GetNext()) {

    $file = [];
    $res = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $elemRes["ID"], ["CODE" => "FILE"]);
    if ($ob = $res->GetNext())
        $file = $ob;

    if ($file) {
        $propFile = CFile::GetFileArray($file["VALUE"]);
        $sizeFile = CFile::FormatSize($propFile["FILE_SIZE"]);
        $pathFile = $propFile["SRC"];

        $arTemp = explode(".", $propFile["ORIGINAL_NAME"]);
        $formatFile = $arTemp[count($arTemp) - 1];

        $elemRes["FILE"] = [
            'size' => $sizeFile ? $sizeFile : '',
            'path' => $pathFile ? $pathFile : '',
            'format' => $formatFile ? $formatFile : '',
        ];
    }
    $arElements[] = $elemRes;
}

foreach ($arSections as $key => $arSection) {
    foreach ($arElements as $key_2 => $arElement) {
        if ($arSection["ID"] == $arElement["IBLOCK_SECTION_ID"]) {
            $arSection["ELEMENTS"][] = $arElement;
        }
    }
    $arSections[$key] = $arSection;
}

foreach ($arSections as $key => $arSection) {
    $items = [];
    foreach ($arSections as $key_2 => $_arSection) {
        if ($arSection["ID"] == $_arSection["IBLOCK_SECTION_ID"]) {
            $items[] = $_arSection;
            unset($arSections[$key_2]);
        }
    }
    if ($items)
        $arSections[$key]["ITEMS"] = $items;
}

$arResult["ITEMS"] = $arSections;

?>