<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-list">
    <? foreach ($arResult["ITEMS"] as $arSection): ?>
        <div class="section">
            <h3><?= $arSection['NAME']; ?></h3>
            <ul>
                <? if ($arSection["ITEMS"]): ?>
                    <? foreach ($arSection["ITEMS"] as $_arSection): ?>
                        <h4><?= $_arSection['NAME']; ?></h4>
                        <li style="list-style: none;">
                            <ul>
                                <? if ($_arSection["ELEMENTS"]): ?>
                                    <? foreach ($_arSection["ELEMENTS"] as $elem): ?>
                                        <li>
                                            <a href="<?= $elem['FILE']['path']; ?>">
                                                <h5><?= $elem['NAME']; ?></h5>
                                            </a>
                                            <span><?= $elem['FILE']['format'] . " / " . $elem['FILE']['size']; ?></span>
                                        </li>
                                    <? endforeach; ?>
                                <? endif; ?>
                            </ul>
                        </li>
                    <? endforeach; ?>
                <? else : ?>
                    <li style="list-style: none;">
                        <ul>
                            <? if ($arSection["ELEMENTS"]): ?>
                                <? foreach ($arSection["ELEMENTS"] as $elem): ?>
                                    <li>
                                        <a href="<?= $elem['FILE']['path']; ?>">
                                            <h5><?= $elem['NAME']; ?></h5>
                                        </a>
                                        <span><?= $elem['FILE']['format'] . " / " . $elem['FILE']['size']; ?></span>
                                    </li>
                                <? endforeach; ?>
                            <? endif; ?>
                        </ul>
                    </li>
                <? endif; ?>
            </ul>
        </div>
    <? endforeach; ?>
</div>
